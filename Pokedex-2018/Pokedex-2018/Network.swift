//
//  Network.swift
//  Pokedex-2018
//
//  Created by Sebastian Guerrero F on 6/13/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import Foundation
import Alamofire

class Network {
  
  func getAllPokemon(completion:@escaping ([Pokemon])->()) {
    var pokemonArray:[Pokemon] = []
    let group = DispatchGroup()
    for i in 1...8 {
      group.enter()
      Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseJSON { response in
        
        guard let data = response.data else {
          print("Error")
          return
        }
        guard let pokemon = try? JSONDecoder().decode(Pokemon.self, from: data) else {
          print("Error decoding Pokemon")
          return
        }
        pokemonArray.append(pokemon)
        group.leave()
      }
    }
    group.notify(queue: .main) {
      completion(pokemonArray)
    }
  }
  
  func getPokemonImage(url:String) {
    
  }
  
}
