//
//  ViewController.swift
//  Pokedex-2018
//
//  Created by Sebastian Guerrero F on 6/13/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
  var pokemon:[Pokemon] = []
  @IBOutlet weak var pokemonTableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let network = Network()
    network.getAllPokemon { (pokemonArray) in
      self.pokemon = pokemonArray
      self.pokemonTableView.reloadData()
    }
  }
  
  //MARK:- TableView
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return pokemon.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = UITableViewCell()
    cell.textLabel?.text = pokemon[indexPath.row].name
    return cell
  }
}
















